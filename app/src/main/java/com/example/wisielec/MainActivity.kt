package com.example.wisielec

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.KeyEvent
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*





class MainActivity : AppCompatActivity() {
    var mytext = ""
    var guessed : ArrayList<Char> = ArrayList()
    var curImage = 0
    var availableNumber = 18
    var wordList : ArrayList<String> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        prepareWordList()
        nextGame()
    }

    private fun prepareWordList() {
        val array = resources.getStringArray(R.array.slowa)
        availableNumber = resources.getStringArray(R.array.slowa).size
        for(w in array){
            wordList.add(w)
        }
    }

    override fun onKeyUp(keyCode: Int, event: KeyEvent): Boolean {
        return when (keyCode) {
            KeyEvent.KEYCODE_ENTER -> {
                if(editText.text.toString().isEmpty())
                    return false
                val letter =  editText.text.toString().get(0)
                for(x in mytext){
                    if(x == letter){
                        guessed.add(letter)
                        fillBlank(mytext)
                        editText.text.clear()

                        return true
                    }
                }
                Toast.makeText(applicationContext, "Źle", Toast.LENGTH_SHORT).show()
                nextImage()

                true
            }
            else -> super.onKeyUp(keyCode, event)
        }
    }


    private fun selectWord(){

        var nr =  0
        if(availableNumber > 1){
            nr = (0..availableNumber).shuffled()[0]
        }
        mytext = wordList.removeAt(nr)
        availableNumber--
        Log.d("myDebug", mytext)
        fillBlank(mytext)
        imageView.setImageResource(R.drawable.wisielec0)
        curImage = 0
    }

    private fun fillBlank (str : String){
        var output = ""
        var wynik = ""
        loop@ for(c in str){
            for(l in guessed){
                if(l == c){
                    output += c
                    wynik += c
                    output += " "
                    continue@loop
                }
            }
            output += "_ "
        }
        word.text = output
        if(wynik.equals(mytext)){
            win()
        }
    }

    private fun win() {
        Toast.makeText(applicationContext, "Wygrana", Toast.LENGTH_SHORT).show()
        nextGame()

    }

    private fun nextGame() {
        Toast.makeText(applicationContext, "Nowa gra", Toast.LENGTH_SHORT).show()
        guessed.clear()
        guessed.add(' ')
        if(availableNumber < 0)
            Toast.makeText(applicationContext, "Brak dostępnych słów. Koniec gry", Toast.LENGTH_SHORT).show()
        else
            selectWord()
    }

    private fun defeat() {
        Toast.makeText(applicationContext, "Przegrana", Toast.LENGTH_SHORT).show()
        nextGame()
    }


    private fun nextImage() {
        curImage++
        if(curImage < 12) {
            imageView.setImageResource(
                resources.getIdentifier(
                    "wisielec" + curImage,
                    "drawable",
                    "com.example.wisielec"
                )
            )
        }
        else
            defeat()

    }
}


